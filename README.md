### Getting Started

Hi there, and welcome to this introduction on Bluebird, and more generally speaking Promises.
A Promise is an object which allows you to write asynchronous code as if it weren’t, keeping a nice readability and propagating errors handling.

Consider this code:

```javascript
    function doSomething(sucess){
        getDataAsync(sucess);
    }
```

which tries to get data from somewhere via an http request, and then triggers an appropriate callback passed as a parameter, on a succeeded operation.
This function can be Promisified as this one:

```javascript
    function promiseDoSomething(){
        return new Promise(function(resolve){
            doSomething(function(data){
                resolve(data);
            })
        });
    }
```

We just need to wrap it around a Promise object, and resolve that promise within the doSomething callback.
Now that your function returns a promise, you can chain it with a then method from a Promise object:

```javascript
    return promiseDoSomething().then(function(data){
        console.log('Here is data from getDataAsync:'+data);
    });
```
Thanks to Promise, the then clause will wait for the returned promise of `promiseDoSomething` to be resolved before being triggered.
I’ll now execute this code, which will display the result of getDataAsync.

Are you ready to chain your first promise ?


For more information, please refer to the documentation: http://bluebirdjs.com/docs/api-reference.html
