var Promise = require("bluebird");
var getDataAsync = require("./getDataAsync");

module.exports = function doSomething() {

    function doSomething(sucess) {
        getDataAsync(sucess);
    }

    function promiseDoSomething() {
        return new Promise(function(resolve) {
            doSomething(function(data) {
                resolve(data);
            })
        });
    }

    return promiseDoSomething().then(function(data) {
        console.log('Here is data from getDataAsync:' + data);
    });

};